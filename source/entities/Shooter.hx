package entities;

import haxepunk.*;
import haxepunk.graphics.*;
import haxepunk.masks.*;
import haxepunk.math.*;
import haxepunk.Tween;
import haxepunk.tweens.misc.*;
import haxepunk.utils.*;
import scenes.*;

class Shooter extends MiniEntity
{
    public static inline var SHOT_COOLDOWN = 1.5;
    public static inline var SHOT_TELL = 0.3;
    public static inline var SHOT_RECOIL = 100;
    public static inline var DECEL = 50;
    public static inline var BOUNCE_FACTOR = 0.9;

    private var sprite:Spritemap;
    private var hitbox:Hitbox;
    private var isActive:Bool;
    private var velocity:Vector2;
    private var shotTell:Alarm;
    private var shotCooldown:Alarm;
    private var shotAngle:Float;

    public function new(x:Float, y:Float) {
        super(x, y);
        type = "enemy";
        hitbox = new Hitbox(24, 24);
        mask = hitbox;
        sprite = new Spritemap("graphics/shooter.png", 24, 24);
        sprite.add("idle", [0]);
        sprite.add("tell", [1]);
        sprite.play("idle");
        graphic = sprite;
        isActive = false;
        velocity = new Vector2();
        shotTell = new Alarm(SHOT_TELL, function() {
            shoot();
        });
        addTween(shotTell);
        shotCooldown = new Alarm(SHOT_COOLDOWN);
        addTween(shotCooldown);
        shotAngle = 0;
    }

    private function shoot() {
        var player = scene.getInstance("player");
        var numBullets = 10;
        shotAngle = (shotAngle + getAngleTowards(player)) / 2;
        for(i in 0...numBullets) {
            var bullet = new Bullet(
                centerX, centerY,
                {
                    width: 8,
                    height: 8,
                    angle: shotAngle,
                    speed: 150 + i * 5,
                    callback: function(b:Bullet) {
                        b.speed = 150 + numBullets * 5;
                    },
                    callbackDelay: 1
                }
            );
            scene.add(bullet);
        }
        shotCooldown.start();
        sprite.play("idle");
        velocity = getHeadingTowards(player);
        velocity.inverse();
        velocity.normalize(SHOT_RECOIL);
    }

    override public function moveCollideX(_:Entity) {
        velocity.x = -velocity.x * BOUNCE_FACTOR;
        return true;
    }

    override public function moveCollideY(_:Entity) {
        velocity.y = -velocity.y * BOUNCE_FACTOR;
        return true;
    }

    override public function update() {
        var player = scene.getInstance("player");
        if(getHeadingTowards(player).length < 150) {
            isActive = true;
        }
        if(isActive) {
            var hasLineOfSight = scene.collideLine(
                "walls",
                Std.int(centerX), Std.int(centerY),
                Std.int(player.centerX), Std.int(player.centerY)
            ) == null;
            if(hasLineOfSight && !shotCooldown.active && !shotTell.active) {
                shotTell.start();
                shotAngle = getAngleTowards(player);
                sprite.play("tell");
            }
            velocity.normalize(MathUtil.approach(
                velocity.length, 0, DECEL * HXP.elapsed
            ));
            moveBy(
                velocity.x * HXP.elapsed, velocity.y * HXP.elapsed,
                ["walls", "enemy"]
            );
        }
        super.update();
    }
}



