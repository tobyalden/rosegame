package entities;

import haxepunk.*;
import haxepunk.graphics.*;
import haxepunk.input.*;
import haxepunk.masks.*;
import haxepunk.math.*;
import haxepunk.Tween;
import haxepunk.tweens.misc.*;
import scenes.*;

class Boomer extends MiniEntity
{
    public static inline var RUN_ACCEL = 2500 / 2;
    public static inline var RUN_ACCEL_TURN_MULTIPLIER = 2;
    public static inline var RUN_DECEL = RUN_ACCEL * RUN_ACCEL_TURN_MULTIPLIER;
    public static inline var AIR_ACCEL = 2000 / 1.5 / 2;
    public static inline var AIR_DECEL = AIR_ACCEL;
    public static inline var MAX_RUN_SPEED = 130 / 2;
    public static inline var MAX_AIR_SPEED = 140 / 2;
    public static inline var GRAVITY = 700 / 2;
    public static inline var GRAVITY_ON_WALL = GRAVITY;
    public static inline var JUMP_POWER = 250 / 1.5;
    public static inline var JUMP_CANCEL_POWER = JUMP_POWER / 2;
    public static inline var WALL_JUMP_POWER_X = 400 / 1.5;
    public static inline var WALL_JUMP_POWER_Y = 250 / 1.5;
    public static inline var WALL_STICKINESS = 1000;
    public static inline var MAX_FALL_SPEED = 300;
    public static inline var MAX_FALL_SPEED_ON_WALL = 100;
    public static inline var WALLJUMP_COOLDOWN = 0.025;
    public static inline var ATTACK_RANGE = 200;
    public static inline var ACTIVATE_RANGE = 150;

    public static inline var CHOICE_COOLDOWN = 0.5;
    public static inline var BONK_HEAD_COOLDOWN = CHOICE_COOLDOWN * 1.5;

    public static inline var ATTACK_TELL = 0.3;

    public static var sfx:Map<String, Sfx> = null;

    public var boomerang(default, null):Boomerang;
    public var sprite(default, null):Spritemap;
    private var velocity:Vector2;
    private var wallJumpCooldown:Alarm;
    private var logicChooser:Alarm;
    private var bonkHeadTimer:Alarm;
    private var isActive:Bool;
    private var attackTell:Alarm;

    private var logicController:Map<String, Bool>;

    private function logicCheck(inputName:String) {
        if(!isActive) {
            return false;
        }
        return logicController[inputName];
    }

    private function logicPressed(inputName:String) {
        if(!isActive) {
            return false;
        }
        if(inputName == "jump" && bonkHeadTimer.active) {
            logicController["jump"] = false;
            return false;
        }
        if(logicController[inputName]) {
            logicController[inputName] = false;
            return true;
        }
        return false;
    }

    private function logicReleased(inputName:String) {
        if(!isActive) {
            return false;
        }
        return logicController[inputName];
    }

    public function new(x:Float, y:Float) {
        super(x, y);
        this.boomerang = new Boomerang(this);
        type = "enemy";
        layer = -3;
        var scaleFactor = 2;
        sprite = new Spritemap(
            "graphics/boomer.png", 8 * scaleFactor, 12 * scaleFactor
        );
        sprite.add("idle", [0]);
        sprite.add("run", [1, 2, 3, 2], 8);
        sprite.add("jump", [4]);
        sprite.add("wall", [5]);
        sprite.add("climb", [5, 7], 4);
        sprite.add("skid", [6]);
        sprite.play("idle");
        mask = new Hitbox(6 * scaleFactor, 12 * scaleFactor);
        sprite.x = -1 * scaleFactor;
        graphic = sprite;
        isActive = false;
        attackTell = new Alarm(ATTACK_TELL, function() {
            boomerang.attack();
        });
        addTween(attackTell);
        velocity = new Vector2();

        wallJumpCooldown = new Alarm(WALLJUMP_COOLDOWN);
        addTween(wallJumpCooldown);

        bonkHeadTimer = new Alarm(BONK_HEAD_COOLDOWN);
        addTween(bonkHeadTimer);

        logicChooser = new Alarm(CHOICE_COOLDOWN, function() {
            var player = scene.getInstance("player");
            if(getHeadingTowards(player).length < ATTACK_RANGE) {
                logicController["attack"] = true;
            }
            for(i in 0...2) {
                var choice = HXP.choose(
                    "turn_away_from_player", "change_direction", "jump",
                    "do_nothing"
                );
                if(choice == "turn_away_from_player") {
                    if(centerX < player.centerX) {
                        logicController["left"] = true;
                        logicController["right"] = false;
                    }
                    else {
                        logicController["right"] = true;
                        logicController["left"] = false;
                    }
                }
                else if(choice == "change_direction") {
                    var oldLeft = logicController["left"];
                    logicController["left"] = logicController["right"];
                    logicController["right"] = oldLeft;
                }
                else if(choice == "jump") {
                    logicController["jump"] = true;
                }
                else if(choice == "do_nothing") {
                    // do nothing
                }
            }
            if(!isOnGround() && isOnWall()) {
                logicController["jump"] = true;
            }
            if(isOnGround()) {
                if(Math.random() > 0.5) {
                    if(isOnLeftWall()) {
                        logicController["right"] = true;
                        logicController["left"] = false;
                    }
                    else if(isOnRightWall()) {
                        logicController["left"] = true;
                        logicController["right"] = false;
                    }
                }
                else {
                    logicController["jump"] = true;
                }
            }
            logicChooser.reset(
                CHOICE_COOLDOWN * HXP.choose(0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3)
            );
        }, TweenType.Persist);
        addTween(logicChooser, true);

        logicController = [
            "left" => false,
            "right" => false,
            "jump" => false,
            "attack" => false
        ];
        if(Math.random() > 0.5) {
            logicController["left"] = true;
        }
        else {
            logicController["right"] = true;
        }
    }

    override public function die() {
        boomerang.die();
        super.die();
    }

    override public function update() {
        var player = cast(scene.getInstance("player"), Player);
        if(
            getHeadingTowards(player).length < ACTIVATE_RANGE
            && (player.isOnGround() || player.isOnWall())
        ) {
            isActive = true;
        }
        movement();
        combat();
        animation();
        super.update();
    }

    private function combat() {
        if(logicPressed("attack")) {
            if(!boomerang.isAttacking && !attackTell.active) {
                attackTell.start();
            }
        }
    }

    private function stopSounds() {
    }

    private function movement() {
        var accel:Float = isOnGround() ? RUN_ACCEL : AIR_ACCEL;
        if(wallJumpCooldown.active) {
            accel *= wallJumpCooldown.percent;
        }
        if(
            isOnGround() && (
                logicCheck("left") && velocity.x > 0
                || logicCheck("right") && velocity.x < 0
            )
        ) {
            accel *= RUN_ACCEL_TURN_MULTIPLIER;
        }
        var decel:Float = isOnGround() ? RUN_DECEL : AIR_DECEL;
        if(wallJumpCooldown.active) {
            decel *= wallJumpCooldown.percent;
        }
        if(logicCheck("left") && !isOnLeftWall()) {
            velocity.x -= accel * HXP.elapsed;
        }
        else if(logicCheck("right") && !isOnRightWall()) {
            velocity.x += accel * HXP.elapsed;
        }
        else if(!isOnWall()) {
            velocity.x = MathUtil.approach(
                velocity.x, 0, decel * HXP.elapsed
            );
        }
        var maxSpeed = isOnGround() ? MAX_RUN_SPEED : MAX_AIR_SPEED;
        velocity.x = MathUtil.clamp(velocity.x, -maxSpeed, maxSpeed);

        if(isOnGround()) {
            velocity.y = 0;
            if(logicPressed("jump")) {
                velocity.y = -JUMP_POWER;
            }
        }
        else if(isOnWall()) {
            var gravity = velocity.y > 0 ? GRAVITY_ON_WALL : GRAVITY;
            velocity.y += gravity * HXP.elapsed;
            velocity.y = Math.min(velocity.y, MAX_FALL_SPEED_ON_WALL);
            if(logicPressed("jump")) {
                velocity.y = -WALL_JUMP_POWER_Y;
                velocity.x = (
                    isOnLeftWall() ? WALL_JUMP_POWER_X : -WALL_JUMP_POWER_X
                );
                sprite.flipX = !sprite.flipX;
                wallJumpCooldown.start();
            }
        }
        else {
            if(logicReleased("jump")) {
                velocity.y = Math.max(velocity.y, -JUMP_CANCEL_POWER);
            }
            velocity.y += GRAVITY * HXP.elapsed;
            velocity.y = Math.min(velocity.y, MAX_FALL_SPEED);
        }

        moveBy(velocity.x * HXP.elapsed, velocity.y * HXP.elapsed, "walls");
    }

    override public function moveCollideX(_:Entity) {
        if(isOnGround()) {
            velocity.x = 0;
        }
        else if(isOnLeftWall()) {
            if(!Input.check("right")) {
                velocity.x = -WALL_STICKINESS;
            }
            velocity.x = Math.max(velocity.x, -WALL_STICKINESS);
        }
        else if(isOnRightWall()) {
            if(!Input.check("left")) {
                velocity.x = WALL_STICKINESS;
            }
            velocity.x = Math.min(velocity.x, WALL_STICKINESS);
        }
        return true;
    }

    override public function moveCollideY(_:Entity) {
        if(velocity.y < 0) {
            bonkHeadTimer.start();
            trace("bonk!");
        }
        velocity.y = 0;
        return true;
    }

    private function animation() {
        if(attackTell.active) {
            sprite.color = 0xF9E927;
        }
        else {
            sprite.color = 0x00F900;
        }
        if(!wallJumpCooldown.active) {
        //if(!wallJumpCooldown.active) {
            if(logicCheck("left")) {
                sprite.flipX = true;
            }
            else if(logicCheck("right")) {
                sprite.flipX = false;
            }
        }

        if(!isOnGround()) {
            if(isOnWall()) {
                if(velocity.y < 0) {
                    sprite.play("climb");
                }
                else {
                    sprite.play("wall");
                }
                sprite.flipX = isOnLeftWall();
            }
            else {
                sprite.play("jump");
            }
        }
        else if(velocity.x != 0) {
            if(
                velocity.x > 0 && logicCheck("left")
                || velocity.x < 0 && logicCheck("right")
            ) {
                sprite.play("skid");

            }
            else {
                sprite.play("run");
            }
        }
        else {
            sprite.play("idle");
        }
    }
}


