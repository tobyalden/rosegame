package entities;

import haxepunk.*;
import haxepunk.graphics.*;
import haxepunk.graphics.tile.*;
import haxepunk.masks.*;
import haxepunk.math.*;
import openfl.Assets;

class Level extends Entity
{
    public static inline var TILE_SIZE = 4;
    public static inline var MIN_LEVEL_WIDTH = 180;
    public static inline var MIN_LEVEL_HEIGHT = 180;
    public static inline var MIN_LEVEL_WIDTH_IN_TILES = 45;
    public static inline var MIN_LEVEL_HEIGHT_IN_TILES = 45;
    public static inline var NUMBER_OF_ROOMS = 4;
    public static inline var NUMBER_OF_HALLWAYS = 2;
    public static inline var NUMBER_OF_SHAFTS = 2;

    public var walls(default, null):Grid;
    public var pathUpWalls(default, null):Grid;
    public var entities(default, null):Array<MiniEntity>;
    private var levelType:String;
    private var tiles:Tilemap;

    public function new(x:Int, y:Int, levelType:String) {
        super(x, y);
        this.levelType = levelType;
        type = "walls";
        if(levelType == "start") {
            loadLevel('0');
        }
        else if(levelType == "room") {
            loadLevel('${
                Std.int(Math.floor(Random.random * NUMBER_OF_ROOMS))
            }');
        }
        else if(levelType == "hallway") {
            loadLevel('${
                Std.int(Math.floor(Random.random * NUMBER_OF_HALLWAYS))
            }');
        }
        else {
            // levelType == "shaft"
            loadLevel('${
                Std.int(Math.floor(Random.random * NUMBER_OF_SHAFTS))
            }');
        }
        if(Random.random < 0.5) {
            flipHorizontally(walls);
            flipHorizontally(pathUpWalls);
            flipEntitiesHorizontally();
        }

        updateGraphic();
        mask = walls;
    }


    override public function update() {
        super.update();
    }

    public function addPathsUp() {
        for(tileX in 0...walls.columns) {
            for(tileY in 0...walls.rows) {
                if(pathUpWalls.getTile(tileX, tileY)) {
                    walls.setTile(tileX, tileY);
                }
            }
        }
    }

    public function flipEntitiesHorizontally() {
        for(entity in entities) {
            entity.x = walls.columns * TILE_SIZE - entity.x;
        }
    }

    public function flipHorizontally(wallsToFlip:Grid) {
        for(tileX in 0...Std.int(wallsToFlip.columns / 2)) {
            for(tileY in 0...wallsToFlip.rows) {
                var tempLeft:Null<Bool> = wallsToFlip.getTile(tileX, tileY);
                // For some reason getTile() returns null instead of false!
                if(tempLeft == null) {
                    tempLeft = false;
                }
                var tempRight:Null<Bool> = wallsToFlip.getTile(
                    wallsToFlip.columns - tileX - 1, tileY
                );
                if(tempRight == null) {
                    tempRight = false;
                }
                wallsToFlip.setTile(tileX, tileY, tempRight);
                wallsToFlip.setTile(
                    wallsToFlip.columns - tileX - 1, tileY, tempLeft
                );
            }
        }
    }

    private function loadLevel(levelName:String) {
        // Load solid geometry
        var xml = Xml.parse(Assets.getText(
            'levels/${levelType}/${levelName}.oel'
        ));
        var fastXml = new haxe.xml.Fast(xml.firstElement());
        var segmentWidth = Std.parseInt(fastXml.node.width.innerData);
        var segmentHeight = Std.parseInt(fastXml.node.height.innerData);
        walls = new Grid(segmentWidth, segmentHeight, TILE_SIZE, TILE_SIZE);
        pathUpWalls = new Grid(
            segmentWidth, segmentHeight, TILE_SIZE, TILE_SIZE
        );
        for (r in fastXml.node.walls.nodes.rect) {
            walls.setRect(
                Std.int(Std.parseInt(r.att.x) / TILE_SIZE),
                Std.int(Std.parseInt(r.att.y) / TILE_SIZE),
                Std.int(Std.parseInt(r.att.w) / TILE_SIZE),
                Std.int(Std.parseInt(r.att.h) / TILE_SIZE)
            );
        }
        if(levelType == "room") {
            for (r in fastXml.node.pathUpWalls.nodes.rect) {
                pathUpWalls.setRect(
                    Std.int(Std.parseInt(r.att.x) / TILE_SIZE),
                    Std.int(Std.parseInt(r.att.y) / TILE_SIZE),
                    Std.int(Std.parseInt(r.att.w) / TILE_SIZE),
                    Std.int(Std.parseInt(r.att.h) / TILE_SIZE)
                );
            }
        }

        // Load optional geometry
        if(fastXml.hasNode.optionalWalls) {
            for (r in fastXml.node.optionalWalls.nodes.rect) {
                if(Random.random < 0.5) {
                    continue;
                }
                walls.setRect(
                    Std.int(Std.parseInt(r.att.x) / TILE_SIZE),
                    Std.int(Std.parseInt(r.att.y) / TILE_SIZE),
                    Std.int(Std.parseInt(r.att.w) / TILE_SIZE),
                    Std.int(Std.parseInt(r.att.h) / TILE_SIZE)
                );
            }
        }

        // Load entities
        entities = new Array<MiniEntity>();
        if(fastXml.hasNode.objects) {
            for(e in fastXml.node.objects.nodes.player) {
                var player = new Player(
                    Std.parseInt(e.att.x), Std.parseInt(e.att.y)
                );
                entities.push(player);
                break;
            }
            for(e in fastXml.node.objects.nodes.enemy) {
                var enemy = new Boomer(
                    Std.parseInt(e.att.x), Std.parseInt(e.att.y)
                );
                entities.push(enemy);
            }
            for(e in fastXml.node.objects.nodes.spike_floor) {
                var spike = new Spike(
                    Std.parseInt(e.att.x), Std.parseInt(e.att.y),
                    Spike.FLOOR, Std.parseInt(e.att.width)
                );
                entities.push(spike);
            }
            for(e in fastXml.node.objects.nodes.spike_ceiling) {
                var spike = new Spike(
                    Std.parseInt(e.att.x), Std.parseInt(e.att.y),
                    Spike.CEILING, Std.parseInt(e.att.width)
                );
                entities.push(spike);
            }
            for(e in fastXml.node.objects.nodes.spike_leftwall) {
                var spike = new Spike(
                    Std.parseInt(e.att.x), Std.parseInt(e.att.y),
                    Spike.LEFT_WALL, Std.parseInt(e.att.height)
                );
                entities.push(spike);
            }
            for(e in fastXml.node.objects.nodes.spike_rightwall) {
                var spike = new Spike(
                    Std.parseInt(e.att.x), Std.parseInt(e.att.y),
                    Spike.RIGHT_WALL, Std.parseInt(e.att.height)
                );
                entities.push(spike);
            }
            for(e in fastXml.node.objects.nodes.checkpoint) {
                var checkpoint = new Checkpoint(
                    Std.parseInt(e.att.x), Std.parseInt(e.att.y)
                );
                entities.push(checkpoint);
            }
            for(e in fastXml.node.objects.nodes.endtrigger) {
                var endTrigger = new EndTrigger(
                    Std.parseInt(e.att.x), Std.parseInt(e.att.y),
                    Std.parseInt(e.att.width), Std.parseInt(e.att.height)
                );
                entities.push(endTrigger);
            }
        }
    }

    public function fillLeft(offsetY:Int) {
        for(tileY in 0...MIN_LEVEL_HEIGHT_IN_TILES) {
            walls.setTile(0, tileY + offsetY * MIN_LEVEL_HEIGHT_IN_TILES);
        }
    }

    public function fillRight(offsetY:Int) {
        for(tileY in 0...MIN_LEVEL_HEIGHT_IN_TILES) {
            walls.setTile(
                walls.columns - 1,
                tileY + offsetY * MIN_LEVEL_HEIGHT_IN_TILES
            );
        }
    }

    public function fillTop(offsetX:Int) {
        for(tileX in 0...MIN_LEVEL_WIDTH_IN_TILES) {
            walls.setTile(tileX + offsetX * MIN_LEVEL_WIDTH_IN_TILES, 0);
            // Clear paths up if not needed
            for(tileY in 0...walls.rows) {
                pathUpWalls.clearTile(
                    tileX + offsetX * MIN_LEVEL_WIDTH_IN_TILES, tileY
                );
            }
        }
    }

    public function fillBottom(offsetX:Int) {
        for(tileX in 0...MIN_LEVEL_WIDTH_IN_TILES) {
            walls.setTile(
                tileX + offsetX * MIN_LEVEL_WIDTH_IN_TILES,
                walls.rows - 1
            );
        }
    }

    public function updateGraphic() {
        tiles = new Tilemap(
            'graphics/stone.png',
            walls.width, walls.height, walls.tileWidth, walls.tileHeight
        );
        for(tileX in 0...walls.columns) {
            for(tileY in 0...walls.rows) {
                if(walls.getTile(tileX, tileY)) {
                    tiles.setTile(tileX, tileY, tileX + tileY * walls.columns);
                }
            }
        }
        graphic = tiles;
    }
}

