package entities;

import haxepunk.*;
import haxepunk.graphics.*;
import haxepunk.masks.*;
import haxepunk.math.*;
import haxepunk.Tween;
import haxepunk.tweens.misc.*;
import haxepunk.utils.*;
import scenes.*;

class Orb extends MiniEntity
{
    public static inline var RADIUS = 12;
    public static inline var ORBIT_RADIUS = 25;
    public static inline var ORBIT_SPEED = 4;
    public static inline var ORBIT_PULSE_AMOUNT = 25;
    //public static inline var ORBIT_PULSE_AMOUNT = 0;
    public static inline var ORBIT_PULSE_SPEED = 1;

    private var sprite:Image;
    private var wielder:MiniEntity;
    private var age:Float;
    private var orbitPulse:NumTween;

    public function new(wielder:MiniEntity) {
        super(0, 0);
        this.wielder = wielder;
        type = "hazard";
        var hitbox = new Circle(RADIUS);
        mask = hitbox;
        sprite = new Image("graphics/orb.png");
        //sprite.x = -RADIUS;
        //sprite.y = -RADIUS;
        graphic = sprite;
        age = Math.random() * Math.PI * 2;
        orbitPulse = new NumTween(TweenType.PingPong);
        addTween(orbitPulse);
        orbitPulse.tween(0, ORBIT_PULSE_AMOUNT, ORBIT_PULSE_SPEED, Ease.sineInOut);
    }

    override public function update() {
        age += HXP.elapsed;
        var orbitAxis = new Vector2(
            wielder.centerX - RADIUS, wielder.centerY - RADIUS
        );
        var orbitArm = new Vector2(ORBIT_RADIUS + orbitPulse.value, 0);
        orbitArm.rotate(age * ORBIT_SPEED);
        moveTo(orbitAxis.x + orbitArm.x, orbitAxis.y + orbitArm.y);
        super.update();
    }

    override public function render(camera:Camera) {
        Draw.lineThickness = 3;
        Draw.line(
            x + RADIUS - scene.camera.x, y + RADIUS - scene.camera.y,
            wielder.centerX - scene.camera.x, wielder.centerY - scene.camera.y
        );
        super.render(camera);
    }
}


