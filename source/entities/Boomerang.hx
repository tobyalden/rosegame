package entities;

import haxepunk.*;
import haxepunk.graphics.*;
import haxepunk.masks.*;
import haxepunk.math.*;
import haxepunk.Tween;
import haxepunk.tweens.misc.*;
import haxepunk.utils.*;
import scenes.*;

class Boomerang extends MiniEntity
{
    public static inline var MAX_SPEED = 300;
    //public static inline var MAX_SPEED = 220;
    //public static inline var RETURN_RATE = 0.35;
    public static inline var RETURN_RATE = 0.75;
    public static inline var RADIUS = 8;

    private var sprite:Image;
    private var wielder:MiniEntity;
    public var isAttacking(default, null):Bool;
    public var age(default, null):Float;

    private var velocity:Vector2;
    private var initialVelocity:Vector2;

    public function new(wielder:MiniEntity) {
        super(0, 0);
        this.wielder = wielder;
        type = "boomerang";
        var hitbox = new Circle(RADIUS);
        mask = hitbox;
        sprite = new Image("graphics/boomerang.png");
        graphic = sprite;
        isAttacking = false;
        velocity = new Vector2();
        initialVelocity = new Vector2();
        age = 0;
    }

    public function attack() {
        if(isAttacking) {
            return;
        }
        isAttacking = true;
        var attackVelocity = getHeadingTowards(scene.getInstance("player"));
        attackVelocity.normalize(MAX_SPEED);
        velocity = attackVelocity;
        initialVelocity = velocity.clone();
    }

    public function counter() {
        if(age <= 0.1) {
            return;
        }
        var player = scene.getInstance("player");
        var awayFromPlayer = getHeadingTowards(player);
        awayFromPlayer.inverse();
        awayFromPlayer.normalize(MAX_SPEED);
        velocity = awayFromPlayer;
        initialVelocity = velocity.clone();
        age = 0;
    }

    override public function update() {
        visible = isAttacking;
        collidable = isAttacking;
        if(isAttacking) {
            //if(age > 0.1 && collide("sword", x, y) != null) {

            //}
            var towardsWielder = new Vector2(
                wielder.centerX - centerX, wielder.centerY - centerY
            );
            var distanceFromWielder = towardsWielder.length;
            towardsWielder.normalize(MAX_SPEED);
            velocity.x = MathUtil.lerp(
                initialVelocity.x,
                towardsWielder.x,
                Math.min(age * RETURN_RATE, 1)
            );
            velocity.y = MathUtil.lerp(
                initialVelocity.y,
                towardsWielder.y,
                Math.min(age * RETURN_RATE, 1)
            );
            towardsWielder.scale(HXP.elapsed);
            if(age > 0.1 && towardsWielder.length >= distanceFromWielder) {
                isAttacking = false;
            }
            else {
                moveBy(velocity.x * HXP.elapsed, velocity.y * HXP.elapsed);
            }
            age += HXP.elapsed;
        }
        else {
            age = 0;
            moveTo(wielder.centerX - RADIUS, wielder.centerY - RADIUS);
        }
        super.update();
    }

    override public function render(camera:Camera) {
        Draw.lineThickness = 3;
        Draw.line(
            x + RADIUS - scene.camera.x, y + RADIUS - scene.camera.y,
            wielder.centerX - scene.camera.x, wielder.centerY - scene.camera.y
        );
        super.render(camera);
    }
}
