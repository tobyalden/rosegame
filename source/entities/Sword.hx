package entities;

import haxepunk.*;
import haxepunk.graphics.*;
import haxepunk.masks.*;
import haxepunk.Tween;
import haxepunk.tweens.misc.*;
import haxepunk.utils.*;
import scenes.*;

class Sword extends MiniEntity
{
    public static inline var ATTACK_DURATION = 0.1;
    public static inline var PLAYER_ATTACK_COOLDOWN = 0.2;
    public static inline var RIVAL_ATTACK_COOLDOWN = 1;

    public var isAttacking(default, null):Bool;
    private var swordSprite:Image;
    private var swordUpSprite:Image;
    private var sprite:Graphiclist;
    private var wielder:MiniEntity;
    private var hitbox:Hitbox;
    private var attackCooldown:Alarm;

    public function new(wielder:MiniEntity) {
        super(0, 0);
        this.wielder = wielder;
        if(Type.getClass(wielder) == Player) {
            type = "sword";
        }
        else if(Type.getClass(wielder) == Rival) {
            type = "hazard";
        }
        hitbox = new Hitbox(26, 28);
        mask = hitbox;
        swordSprite = new Image("graphics/sword.png");
        swordUpSprite = new Image("graphics/sword_up.png");
        sprite = new Graphiclist([swordSprite, swordUpSprite]);
        graphic = sprite;
        isAttacking = false;
        attackCooldown = new Alarm(
            Type.getClass(wielder) == Rival
            ? RIVAL_ATTACK_COOLDOWN : PLAYER_ATTACK_COOLDOWN
        );
        addTween(attackCooldown);
    }

    public function canAttack() {
         return !isAttacking && !attackCooldown.active;
    }

    public function attack() {
        if(!canAttack()) {
            return;
        }
        if(Type.getClass(wielder) == Player) {
            Player.sfx["attack"].play();
        }
        isAttacking = true;
        HXP.alarm(ATTACK_DURATION, function() {
            isAttacking = false;
            attackCooldown.start();
        });
    }

    override public function update() {
        sprite.visible = isAttacking;
        collidable = isAttacking;
        var wielderFlipped = false;
        if(Type.getClass(wielder) == Player) {
            wielderFlipped = cast(wielder, Player).sprite.flipX;
        }
        else if(Type.getClass(wielder) == Rival) {
            wielderFlipped = cast(wielder, Rival).sprite.flipX;
        }
        if(!isAttacking) {
            swordSprite.flipX = wielderFlipped;
            if(wielder.isOnWall()) {
                swordSprite.flipX = !swordSprite.flipX;
            }
        }
        if(Main.inputCheck("down")) {
            swordSprite.alpha = 0;
            swordUpSprite.alpha = 1;
            swordUpSprite.flipX = swordSprite.flipX;
            swordUpSprite.flipY = true;
            moveTo(wielder.x - 3, wielder.bottom - 8);
            hitbox.width = 18;
            hitbox.height = 24;
            hitbox.x = 0;
            hitbox.y = 8;
        }
        else if(Main.inputCheck("up")) {
            swordSprite.alpha = 0;
            swordUpSprite.alpha = 1;
            swordUpSprite.flipX = swordSprite.flipX;
            swordUpSprite.flipY = false;
            moveTo(wielder.x - 3, wielder.y - 25);
            hitbox.width = 18;
            hitbox.height = 24;
            hitbox.x = 0;
            hitbox.y = 0;
        }
        else {
            swordSprite.alpha = 1;
            swordUpSprite.alpha = 0;
            hitbox.width = 26;
            hitbox.height = 28;
            moveTo(wielder.x + (swordSprite.flipX ? -19 : 0), wielder.y - 4);
            hitbox.x = swordSprite.flipX ? 0 : 7;
            hitbox.y = 0;
        }
        super.update();
    }
}

